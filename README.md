# fluxrun

Python wrapper for the EddyPro flux calculation software

The source code for `fluxrun` can now be found here: [fluxrun source code](https://gitlab.ethz.ch/flux/fluxrun)
